# Copyright (c) 2024 1001 Lakes Oy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 

from typing import Optional, List
from pydantic import ConfigDict, BaseModel, Field
from pydantic.functional_validators import BeforeValidator

from typing_extensions import Annotated
from bson import ObjectId

# Represents an ObjectId field in the database.
# It will be represented as a `str` on the model so that it can be serialized to JSON.
PyObjectId = Annotated[str, BeforeValidator(str)]

class PolicyModel(BaseModel):
    """
    A policy document in the database.
    """
    id: Optional[PyObjectId] = Field(alias="_id", default=None)
    title: str = Field(...)
    description: str = Field(...)
    policy: str = Field(...)
    model_config = ConfigDict(
        populate_by_name=True,
        arbitrary_types_allowed=True
    )

class UpdatePolicyModel(BaseModel):
    """
    A set of optional updates to be made to a policy document in the database.
    """
    title: Optional[str] = None
    description: Optional[str] = None
    policy: Optional[str] = None
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        json_encoders={ObjectId: str}
    )

class PolicyCollection(BaseModel):
    """
    A container holding a list of `PolicyModel` instances.

    This exists because providing a top-level array in a JSON response can be a [vulnerability](https://haacked.com/archive/2009/06/25/json-hijacking.aspx/)
    """
    policies: List[PolicyModel]

class TemplateModel(BaseModel):
    """
    A policy template in the database.
    """
    id: Optional[PyObjectId] = Field(alias="_id", default=None)
    title: str = Field(...)
    description: str = Field(...)
    type: str = Field(...)
    template: str = Field(...)
    params: dict = Field(...)
    model_config = ConfigDict(
        populate_by_name=True,
        arbitrary_types_allowed=True
    )

class UpdateTemplateModel(BaseModel):
    """
    A set of optional updates to be made to a policy template in the database.
    """
    title: Optional[str] = None
    description: Optional[str] = None
    type: Optional[str] = None
    template: Optional[str] = None
    params: Optional[dict] = None
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        json_encoders={ObjectId: str}
    )

class TemplateCollection(BaseModel):
    """
    A container holding a list of `TemplateModel` instances.
    """
    templates: List[TemplateModel]

class PermissionModel(BaseModel):
    """
    Structure for the ODRL Permission class.
    """
    template_id: str = Field(...)
    target: Optional[str] = Field(...)
    assigner: Optional[str] = Field(...)
    assignee: Optional[str] = Field(...)
    action: str = Field(...)
    constraints: Optional[List[dict]] = Field(default=[])
    duties: Optional[List[dict]] = Field(default=[])
    model_config = ConfigDict(
        populate_by_name=True,
        arbitrary_types_allowed=True
    )

class ProhibitionModel(BaseModel):
    """
    Structure for the ODRL Prohibition class.
    """
    template_id: str = Field(...)
    target: Optional[str] = Field(...)
    assigner: Optional[str] = Field(...)
    assignee: Optional[str] = Field(...)
    action: str = Field(...)
    constraints: Optional[List[dict]] = Field(default=[])
    remedies: Optional[List[dict]] = Field(default=[])
    model_config = ConfigDict(
        populate_by_name=True,
        arbitrary_types_allowed=True
    )

class DutyModel(BaseModel):
    """
    Structure for the ODRL Duty class.
    """
    template_id: str = Field(...)
    target: Optional[str] = Field(...)
    assigner: Optional[str] = Field(...)
    assignee: Optional[str] = Field(...)
    action: str = Field(...)
    constraints: Optional[List[dict]] = Field(default=[])
    consequences: Optional[List[dict]] = Field(default=[])
    model_config = ConfigDict(
        populate_by_name=True,
        arbitrary_types_allowed=True
    )

class ParamDictModel(BaseModel):
    """
    Defines a structure for delivering dynamic parameters for template-based policy creation.
    """
    uid: str = Field(...)
    title: str = Field(...)
    description: str = Field(...)
    type: str = Field(...)
    permissions: List[dict] = Field(default=[])
    prohibitions: List[dict] = Field(default=[])
    obligations: List[dict] = Field(default=[])
    model_config = ConfigDict(
        populate_by_name=True,
        arbitrary_types_allowed=True
    )
