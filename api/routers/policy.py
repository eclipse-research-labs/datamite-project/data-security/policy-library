# Copyright (c) 2024 1001 Lakes Oy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 

from bson import ObjectId
from dotenv import load_dotenv
from fastapi import APIRouter, Body, HTTPException, status, Request
from jinja2 import Environment, FileSystemLoader
import motor.motor_asyncio
import os
from pymongo import ReturnDocument

from models import PolicyModel, UpdatePolicyModel, PolicyCollection, ParamDictModel


router = APIRouter()
load_dotenv()

path = os.path.dirname(os.path.abspath(__file__))
jinja_env = Environment(
    loader=FileSystemLoader(os.path.join(path, "templates/")),
    trim_blocks=True,
    lstrip_blocks=True,
)

# Connect to MongoDB
mongo_client = motor.motor_asyncio.AsyncIOMotorClient(os.environ.get("MONGODB_URL"))
db = mongo_client.get_database("PolicyLibrary")
collection = db.get_collection("policies")
template_collection = db.get_collection("templates")


# Get a policy by ID from MongoDB
@router.get(
        "/policies/{id}",
        response_description="Get policy by id",
        response_model=PolicyModel,
        response_model_by_alias=False,
        tags=["policies"]
)
async def get_policy_by_id(id: str):

    try:
        policy = await collection.find_one({"_id": ObjectId(id)})
        if policy:
            return policy
        else:
            raise HTTPException(status_code=404)
    
    except Exception as e:
        if "404" in str(e):
            raise HTTPException(status_code=404, detail=f"Policy '{id}' not found")
        else:
            raise HTTPException(status_code=500, detail=f"Error: {str(e)}")
    

# Get all policy titles from MongoDB
@router.get(
        "/policies",
        response_description="Get all policies",
        response_model=PolicyCollection,
        response_model_by_alias=False,
        tags=["policies"]
)
async def get_all_policies(request: Request):
    """
    List all of the student data in the database.

    The response is unpaginated and limited to 1000 results.
    """
    return PolicyCollection(policies = await collection.find().to_list(1000))


# Post a policy via request body to MongoDB
@router.post(
        "/policies",
        response_description="Add new policy",
        response_model=PolicyModel,
        status_code=status.HTTP_201_CREATED,
        response_model_by_alias=False,
        tags=["policies"]
)
async def post_policy(request_body: PolicyModel = Body(...)):
    
    try:
        title = request_body.title
        existing = await collection.find_one({"title": title})
        if existing:
            raise HTTPException(status_code=400)
        else:
            new_policy = await collection.insert_one(
                request_body.model_dump(by_alias=True, exclude=["id"])
            )
            created_policy = await collection.find_one(
                {"_id": new_policy.inserted_id}
            )
            return created_policy
        
    except Exception as e:
        if "400" in str(e):
            raise HTTPException(status_code=400, detail=f"A policy with title '{title}' already exists")
        else:
            raise HTTPException(status_code=500, detail=f"Error: {str(e)}")


# Post a policy via templates to MongoDB
@router.post(
        "/policies/template",
        response_description="Add new policy from templates",
        response_model=PolicyModel,
        status_code=status.HTTP_201_CREATED,
        response_model_by_alias=False,
        tags=["policies"]
)
async def post_policy_with_templates(request_body: ParamDictModel = Body(...)):
    
    try:
        print(request_body)
        title = request_body.title
        existing = await collection.find_one({"title": title})
        if existing:
            raise HTTPException(status_code=400)
        else:
            master = await template_collection.find_one({"_id": ObjectId("66c6f0b809872f82ef9fb737")})
            master_template = jinja_env.from_string(master["template"])
            
            # Render permission block
            permissions = "["

            for permission in request_body.permissions:
                permission_template_dict = await template_collection.find_one({"_id": ObjectId(permission["template_id"])})
                permission_template = jinja_env.from_string(permission_template_dict["template"])

                # Render constraints
                constraints = "["
                for constraint in permission["constraints"]:
                    constraint_template_dict = await template_collection.find_one({"_id": ObjectId(constraint["template_id"])})
                    constraint_template = jinja_env.from_string(constraint_template_dict["template"])
                    constraints = constraints + constraint_template.render(constraint["param_dict"])

                constraints = constraints + "]"
                permission_params = {
                    "target": permission["target"],
                    "assigner": permission["assigner"],
                    "assignee": permission["assignee"],
                    "action": permission["action"],
                    "constraints": constraints,
                }
                perm = permission_template.render(permission_params)
                permissions = permissions + perm

            permissions = permissions + "]"

            master_params = {
                "type": request_body.type,
                "uid": request_body.uid,
                "permissions": permissions,
            }
            res = master_template.render(master_params)

            policy_document = PolicyModel(
                title=title,
                description=request_body.description,
                policy=res
            )

            insert_result = await collection.insert_one(policy_document.model_dump(by_alias=True, exclude=["id"]))

            created_policy = await collection.find_one(
                {"_id": insert_result.inserted_id}
            )
            return created_policy
        
    except Exception as e:
        if "400" in str(e):
            raise HTTPException(status_code=400, detail=f"A policy with title '{title}' already exists")
        else:
            raise HTTPException(status_code=500, detail=f"Error: {str(e)}")


# Update a policy by id in MongoDB
@router.put(
        "/policies/{id}",
        response_description="Update policy",
        response_model=PolicyModel,
        response_model_by_alias=False,
        tags=["policies"]
)
async def update_policy_by_id(id: str, updates: UpdatePolicyModel = Body(...)):
    """
    Update individual fields of an existing policy record.

    Only the provided fields will be updated.
    Any missing or `null` fields will be ignored.
    """
    policy = {
        k: v for k, v in updates.model_dump(by_alias=True).items() if v is not None
    }

    if len(policy) >= 1:
        update_result = await collection.find_one_and_update(
            {"_id": ObjectId(id)},
            {"$set": policy},
            return_document=ReturnDocument.AFTER,
        )
        if update_result is not None:
            return update_result
        else:
            raise HTTPException(status_code=404, detail=f"Policy {id} not found")

    # The update is empty, but we should still return the matching document:
    if (existing_policy := await collection.find_one({"_id": id})) is not None:
        return existing_policy

    raise HTTPException(status_code=404, detail=f"Policy {id} not found")


# Delete a policy by ID from MongoDB
@router.delete(
        "/policies/{id}",
        response_description="Delete policy by ID",
        status_code=status.HTTP_204_NO_CONTENT,
        tags=["policies"]
)
async def delete_policy_by_title(id: str):

    try:
        result = await collection.delete_one({"_id": ObjectId(id)})
        if result.deleted_count == 0:
            raise HTTPException(status_code=404)
        
    except Exception as e:
        if "404" in str(e):
            raise HTTPException(status_code=404, detail=f"Policy {id} not found")
        else:
            raise HTTPException(status_code=500, detail=f"Error: {str(e)}")

