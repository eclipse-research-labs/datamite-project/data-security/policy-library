# Copyright (c) 2024 1001 Lakes Oy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 

from bson import ObjectId
from dotenv import load_dotenv
from fastapi import APIRouter, Body, HTTPException, status
import motor.motor_asyncio
import os
from pymongo import ReturnDocument

from models import TemplateModel, UpdateTemplateModel, TemplateCollection


router = APIRouter()
load_dotenv()
path = os.path.dirname(os.path.abspath(__file__))

# Connect to MongoDB
mongo_client = motor.motor_asyncio.AsyncIOMotorClient(os.environ.get("MONGODB_URL"))
db = mongo_client.get_database("PolicyLibrary")
collection = db.get_collection("templates")


# Get a template by ID from MongoDB
@router.get(
        "/templates/{id}",
        response_description="Get template by id",
        response_model=TemplateModel,
        response_model_by_alias=False,
        tags=["templates"]
)
async def get_template_by_id(id: str):

    try:
        template = await collection.find_one({"_id": ObjectId(id)})
        if template:
            return template
        else:
            raise HTTPException(status_code=404)
    
    except Exception as e:
        if "404" in str(e):
            raise HTTPException(status_code=404, detail=f"Template '{id}' not found")
        else:
            raise HTTPException(status_code=500, detail=f"Error: {str(e)}")
        

# Get all templates from MongoDB
@router.get(
        "/templates",
        response_description="Get all templates",
        response_model=TemplateCollection,
        response_model_by_alias=False,
        tags=["templates"]
)
async def get_all_templates():
    """
    List all of the template data in the database.

    The response is unpaginated and limited to 1000 results.
    """
    return TemplateCollection(templates = await collection.find().to_list(1000))


# Get a template params dict by ID from MongoDB
@router.get(
    "/template_params/{id}",
    response_description="Get template params by id",
    response_model=dict,
    tags=["templates"]
)
async def get_template_params_by_id(id: str):
    try:
        template = await collection.find_one({"_id": ObjectId(id)})
        if template:
            res = {"id": str(template.get("_id", "")), "title": template.get("title", ""), "type": template.get("type", ""), "params": template.get("params", {})}
            return res
        else:
            raise HTTPException(status_code=404)
    
    except Exception as e:
        if "404" in str(e):
            raise HTTPException(status_code=404, detail=f"Template '{id}' not found")
        else:
            raise HTTPException(status_code=500, detail=f"Error: {str(e)}")
    

# Get all template params dicts from MongoDB
@router.get(
        "/template_params",
        response_description="Get all template params dicts",
        response_model=list[dict],
        response_model_by_alias=False,
        tags=["templates"]
)
async def get_all_template_params_dicts():
    """
    Get all of the template param dicts in the database.

    The response is unpaginated and limited to 1000 results.
    """
    try:
        templates = await collection.find().to_list(1000)
        res = [ {"id": str(template.get("_id", "")), "title": template.get("title", ""), "type": template.get("type", ""), "params": template.get("params", {})} for template in templates ]
        return res
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Error: {str(e)}")


# Post a template via request body to MongoDB
@router.post(
        "/templates",
        response_description="Add new template",
        response_model=TemplateModel,
        status_code=status.HTTP_201_CREATED,
        response_model_by_alias=False,
        tags=["templates"]
)
async def post_template(request_body: TemplateModel = Body(...)):
    
    try:
        title = request_body.title
        existing = await collection.find_one({"title": title})
        if existing:
            raise HTTPException(status_code=400)
        else:
            new_template = await collection.insert_one(
                request_body.model_dump(by_alias=True, exclude=["id"])
            )
            created_template = await collection.find_one(
                {"_id": new_template.inserted_id}
            )
            return created_template
        
    except Exception as e:
        if "400" in str(e):
            raise HTTPException(status_code=400, detail=f"A template with title '{title}' already exists")
        else:
            raise HTTPException(status_code=500, detail=f"Error: {str(e)}")
        

# Post a template via txt file to MongoDB
@router.post(
        "/templates/file/{file_name}",
        response_description="Add new template from file",
        response_model=TemplateModel,
        status_code=status.HTTP_201_CREATED,
        response_model_by_alias=False,
        tags=["templates"]
)
async def post_template_from_file(file_name: str, request_body: TemplateModel = Body(...)):
    try:
        title = request_body.title
        existing = await collection.find_one({"title": title})
        if existing:
                raise HTTPException(status_code=400)
        else:
            with open(f"{path}/templates/{file_name}.txt", "r") as file:
                template = file.read()
                request_body.template = template

                new_template = await collection.insert_one(
                    request_body.model_dump(by_alias=True, exclude=["id"])
                )
                created_template = await collection.find_one(
                    {"_id": new_template.inserted_id}
                )
                return created_template
        
    except Exception as e:
        if "400" in str(e):
            raise HTTPException(status_code=400, detail=f"A template with title '{title}' already exists")
        else:
            raise HTTPException(status_code=500, detail=f"Error: {str(e)}")


# Update a template by id in MongoDB
@router.put(
        "/templates/{id}",
        response_description="Update template",
        response_model=TemplateModel,
        response_model_by_alias=False,
        tags=["templates"]
)
async def update_template_by_id(id: str, updates: UpdateTemplateModel = Body(...)):
    """
    Update individual fields of an existing template record.

    Only the provided fields will be updated.
    Any missing or `null` fields will be ignored.
    """
    template = {
        k: v for k, v in updates.model_dump(by_alias=True).items() if v is not None
    }

    if len(template) >= 1:
        update_result = await collection.find_one_and_update(
            {"_id": ObjectId(id)},
            {"$set": template},
            return_document=ReturnDocument.AFTER,
        )
        if update_result is not None:
            return update_result
        else:
            raise HTTPException(status_code=404, detail=f"template {id} not found")

    # The update is empty, but we should still return the matching document:
    if (existing_template := await collection.find_one({"_id": id})) is not None:
        return existing_template

    raise HTTPException(status_code=404, detail=f"Template {id} not found")


# Delete a template by ID from MongoDB
@router.delete(
        "/templates/{id}",
        response_description="Delete template by ID",
        status_code=status.HTTP_204_NO_CONTENT,
        tags=["templates"]
)
async def delete_template_by_title(id: str):

    try:
        result = await collection.delete_one({"_id": ObjectId(id)})
        if result.deleted_count == 0:
            raise HTTPException(status_code=404)
        
    except Exception as e:
        if "404" in str(e):
            raise HTTPException(status_code=404, detail=f"Template {id} not found")
        else:
            raise HTTPException(status_code=500, detail=f"Error: {str(e)}")

