<!--
  ~ Copyright (c) 2024 1001 Lakes Oy
  ~ 
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of
  ~ this software and associated documentation files (the "Software"), to deal in
  ~ the Software without restriction, including without limitation the rights to
  ~ use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  ~ the Software, and to permit persons to whom the Software is furnished to do so,
  ~ subject to the following conditions:
  ~ 
  ~ The above copyright notice and this permission notice shall be included in all
  ~ copies or substantial portions of the Software.
  ~ 
  ~ THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  ~ IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  ~ FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  ~ COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  ~ IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  ~ CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~ 
  ~ SPDX-License-Identifier: MIT
  ~ 
-->

# policy-library


## Description
The DATAMITE Policy Library provides a collection of ODRL policy templates, on top of which users can build their custom policies. A database for storing both templates and custom policies, mapped to human interpretable explanations and common contracts (e.g. Rulebook), will be provided. Policy orchestration will be done through a simple user interface. The Policy Library is developed and managed by 1001LAKES.

<img src="architecture.png" alt="architecture" style="width:50%;height:auto;">

NOTE: At this point, the architecture and functionalities might still be subject to change due to open questions regarding division of responsibilities with other tools.

## Installation
Docker Compose. Compose file will be provided later.


## Requirements
TBD


## Usage
- REST API interface for now, UI will be provided before M24. Policy CRUD & Template CRUD.


## Roadmap M24
- [x] At least one E2E implemented restriction feature
- [x] Policy template CRUD API
- [ ] Basic UI for ODRL policy orchestration
- [ ] Integration to TEC policy engine


## Support
- Joel Himanen (repository owner), joel.himanen@1001lakes.com
- Sami Jokela, sami.jokela@1001lakes.com


## License
This component is provided under the terms of the **Eclipse Public License - v 2.0**.
