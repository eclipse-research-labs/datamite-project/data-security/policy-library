# Copyright (c) 2024 1001 Lakes Oy
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# SPDX-License-Identifier: MIT
# 

import mesop as me

from dataclasses import field
import json
import requests


@me.stateclass
class State:
    show_permission_form: bool = False
    dynamic_data: dict = field(default_factory=lambda: {
        "uid": "",
        "title": "",
        "description": "",
        "type": "",
        "permissions": [],
        "prohibitions": [],
        "obligations": [],
    })
    selected_permission_constraints: list[str]
    view_code: bool
    submitted_policy: str = ""


def is_mobile():
    return me.viewport_size().width < 640

PARAM_MAP = {
    "Time interval": {
        "id": "66e9590fb68a777a53b23fc5",
        "params": {
            "lower_bound": "",
            "upper_bound": "",
        },
    }
}

###########################
### MAIN PAGE STRUCTURE ###
###########################

@me.page(path="/")
def page():
    s = me.state(State)

    with me.box(style=me.Style(display="grid", grid_template_rows="auto 1fr auto", height="100vh", background="#f0f4f8")):
        
        # HEADER #
        with me.box(
            style=me.Style(
                display="flex",
                flex_direction="column" if is_mobile() else "row",
                padding=me.Padding.all(24),
                justify_content="center",
                background="#1a365d",
                color="white",
            )
        ):
            header_text()
        
        # BODY #
        with me.box(
            style=me.Style(padding=me.Padding.all(24), overflow_y="auto")
        ):
            with flexbox(
                flex_direction="column",
                flex_wrap="wrap",
                justify_content="space-evenly",
                border=me.Border.all(me.BorderSide(width=2, color="#4a5568", style="solid")),
                border_radius=10,
                background="white",
                padding=me.Padding.all(20),
            ):
                with flexbox(margin=me.Margin.all(0)):
                    me.text(
                        "General information",
                        type="headline-5",
                        style=me.Style(font_weight=700, padding=me.Padding(top=10, bottom=20), color="#2c5282")
                    )
                with flexbox(
                    flex_wrap="wrap",
                    justify_content="space-around",
                ):
                    input_style = me.Style(margin=me.Margin(bottom=15), width=200)
                    me.select(
                        label="Policy type",
                        options=[
                            me.SelectOption(label="Set", value="Set"),
                            me.SelectOption(label="Offer", value="Offer"),
                            me.SelectOption(label="Agreement", value="Agreement"),
                        ],
                        on_selection_change=on_type_selection_change,
                        style=input_style
                    )
                    me.input(label="Policy UID", on_blur=on_uid_blur, style=input_style)
                    me.input(label="Policy title", on_blur=on_title_blur, style=input_style)
                    me.input(label="Policy description", on_blur=on_description_blur, style=input_style)
            
            # Permissions
            if s.show_permission_form:
                with flexbox(
                    flex_direction="column",
                    flex_wrap="wrap",
                    justify_content="space-evenly",
                    border=me.Border.all(me.BorderSide(width=2, color="#4a5568", style="solid")),
                    border_radius=10,
                    background="white",
                    padding=me.Padding.all(20),
                    margin=me.Margin(top=20),
                ):
                    with flexbox(margin=me.Margin.all(0)):
                        me.text(
                            "Permission",
                            type="headline-5",
                            style=me.Style(font_weight=700, padding=me.Padding(top=10, bottom=20), color="#2c5282")
                        )
                    permission_form()
            
            with flexbox(margin=me.Margin(top=20)):
                button("+ Add permission", "#4299e1", on_add_permission_click)

            with flexbox(margin=me.Margin(top=20), gap=50):
                view_code_checkbox()
                button("Submit", "#48bb78", on_submit_click)

            # Render submitted policy
            if s.submitted_policy:
                with flexbox(
                    flex_direction="column",
                    border=me.Border.all(me.BorderSide(width=2, color="#4a5568", style="solid")),
                    border_radius=10,
                    background="white",
                    gap=0,
                    padding=me.Padding.all(20),
                    margin=me.Margin(top=20),
                ):
                    with flexbox(margin=me.Margin.all(0)):
                        me.text(
                            "Submitted Policy",
                            type="headline-5",
                            style=me.Style(font_weight=700, padding=me.Padding(top=10, bottom=20), color="#2c5282")
                        )
                    with flexbox(margin=me.Margin.all(0)):
                        me.text(s.submitted_policy, style=me.Style(white_space="pre-wrap"))

        # FOOTER #
        with me.box(
            style=me.Style(padding=me.Padding.all(24), background="#1a365d", color="white", text_align="center")
        ):
            me.text("DATAMITE Policy Orchestration Tool")

##################
### COMPONENTS ###
##################

# HEADER #

def header_text():
    me.text(
        "DATAMITE Policy Orchestration Tool",
        style=me.Style(
            font_size=36,
            font_weight=700,
        ),
    )

# BODY #

def on_type_selection_change(e: me.SelectSelectionChangeEvent):
    s = me.state(State)
    s.dynamic_data["type"] = e.value

def on_uid_blur(e: me.InputBlurEvent):
    s = me.state(State)
    s.dynamic_data["uid"] = e.value

def on_title_blur(e: me.InputBlurEvent):
    s = me.state(State)
    s.dynamic_data["title"] = e.value

def on_description_blur(e: me.InputBlurEvent):
    s = me.state(State)
    s.dynamic_data["description"] = e.value

# Permissions

def permission_form():
    s = me.state(State)

    with flexbox(flex_wrap="wrap"):
        input_style = me.Style(margin=me.Margin(bottom=15), width=200)
        me.input(label="Target", on_blur=on_target_blur, style=input_style)
        me.input(label="Assigner", on_blur=on_assigner_blur, style=input_style)
        me.input(label="Assignee", on_blur=on_assignee_blur, style=input_style)
        me.input(label="Action", on_blur=on_action_blur, style=input_style)
    
    if s.dynamic_data["permissions"][0]["constraints"]:
        with flexbox(flex_direction="column", gap=None):
            if s.dynamic_data["permissions"][0]["constraints"][0]["template_id"] == "66e9590fb68a777a53b23fc5":
                with flexbox(gap=None, margin=None):
                    me.text("Time interval constraint", type="headline-6", style=me.Style(color="#2c5282", margin=me.Margin(bottom=10)))
                with flexbox(margin=None):
                    # me.date_picker(
                    #     label="Date",
                    #     disabled=False,
                    #     placeholder="9/1/2024",
                    #     required=True,
                    #     value=state.picked_date,
                    #     readonly=False,
                    #     hide_required_marker=False,
                    #     color="accent",
                    #     float_label="always",
                    #     appearance="outline",
                    #     on_change=on_date_change,
                    # )
                    me.input(label="Lower bound", on_blur=on_lower_bound_blur, style=input_style)
                    me.input(label="Upper bound", on_blur=on_upper_bound_blur, style=input_style)

    with flexbox(margin=me.Margin(top=20)):
        constraint_selector()
        button("+ Add constraint", "#4299e1", on_add_constraint_click)

def on_target_blur(e: me.InputBlurEvent):
    s = me.state(State)
    s.dynamic_data["permissions"][0]["target"] = e.value

def on_assigner_blur(e: me.InputBlurEvent):
    s = me.state(State)
    s.dynamic_data["permissions"][0]["assigner"] = e.value

def on_assignee_blur(e: me.InputBlurEvent):
    s = me.state(State)
    s.dynamic_data["permissions"][0]["assignee"] = e.value

def on_action_blur(e: me.InputBlurEvent):
    s = me.state(State)
    s.dynamic_data["permissions"][0]["action"] = e.value

# def on_date_change(e: me.DatePickerChangeEvent):
#   s = me.state(State)
#   s.dynamic_data["permissions"][0]["constraints"][0]["param_dict"]["lower_bound"] = e.date

def on_lower_bound_blur(e: me.InputBlurEvent):
    s = me.state(State)
    s.dynamic_data["permissions"][0]["constraints"][0]["param_dict"]["lower_bound"] = e.value

def on_upper_bound_blur(e: me.InputBlurEvent):
    s = me.state(State)
    s.dynamic_data["permissions"][0]["constraints"][0]["param_dict"]["upper_bound"] = e.value

def constraint_selector():
    me.select(
        label="Choose constraint",
        options=[
            me.SelectOption(label="Time interval", value="Time interval"),
        ],
        on_selection_change=on_constraint_selection_change,
        style=me.Style(width=200, margin=me.Margin(right=10)),
    )

def on_constraint_selection_change(e: me.SelectSelectionChangeEvent):
    s = me.state(State)
    s.selected_permission_constraints = e.values

def on_add_constraint_click(e: me.ClickEvent):
    s = me.state(State)
    title = s.selected_permission_constraints[0]
    id = PARAM_MAP[title]["id"]
    params = PARAM_MAP[title]["params"]
    s.dynamic_data["permissions"][0]["constraints"].append({"template_id": id, "param_dict": params})

def on_add_permission_click(e: me.ClickEvent):
    s = me.state(State)
    permission_param_dict = {"template_id":"66e2e890f85bfecb95e8f4a9", "target":"", "assigner":"", "assignee":"", "action":"", "constraints":[]}
    s.dynamic_data["permissions"].append(permission_param_dict)
    s.show_permission_form = True


# Submit

def view_code_checkbox():
    me.checkbox(
        "View policy before submitting",
        on_change=on_update,
        style=me.Style(margin=me.Margin(right=20)),
    )

def on_update(event: me.CheckboxChangeEvent):
    s = me.state(State)
    s.view_code = event.checked

def on_submit_click(e: me.ClickEvent):
    s = me.state(State)
    response = requests.post(
        "http://localhost:80/policies/template",
        json=s.dynamic_data,
        headers={"Content-Type": "application/json"}
    ).json()
    s.submitted_policy = response["policy"]

########################
### GENERIC ELEMENTS ###
########################

def flexbox(
    background=None,
    flex_direction="row",
    flex_wrap="None",
    justify_content="center",
    margin=me.Margin(bottom=30),
    gap=30,
    border=None,
    border_radius=None,
    padding=None,
):
    box = me.box(
        style=me.Style(
            display="flex",
            background=background,
            flex_direction=flex_direction,
            flex_wrap=flex_wrap,
            justify_content=justify_content,
            margin=margin,
            gap=gap,
            border=border,
            border_radius=border_radius,
            padding=padding,
        )
    )
    return box

def button(label: str, background: str, on_click):
    return me.button(
        label,
        style=me.Style(
            background=background,
            color="#fff",
            padding=me.Padding.symmetric(horizontal=15, vertical=10),
            border_radius=4,
            width=150,
        ),
        on_click=on_click,
    )
